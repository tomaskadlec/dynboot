"""
    wsgi.py
    Application entrypoint
"""

import http.cookies
import urllib.parse

import dynboot
dynboot.initialize("wsgi")


class DynbootError(Exception):
    status = "400 Bad Request"

    pass


class EmptyAddressError(DynbootError):
    message = "Address is empty."

    def __init__(self):
        super().__init__(self.message.format())


class NoSuchAddressError(DynbootError):
    message = "Address {} does not exist."
    status = "404 Not Found"

    def __init__(self, address=''):
        super().__init__(self.message.format(address))


class NoSuchMenuError(DynbootError):
    message = "Menu {} does not exists."
    status = "500 Internal Server error"

    def __init__(self, menu=''):
        super().__init__(self.message.format(menu))


def application(environ, start_fn):

    try:
        address = _address(environ)
        if not address:
            raise EmptyAddressError

        menu_identifier = _resolve(address)
        menu = _locate(menu_identifier)

        start_fn('200 OK', [('Content-Type', 'text/plain')])
        dynboot.logger.debug("Result: %s", menu)
        yield menu.encode("utf-8")
    except DynbootError as e:
        start_fn(e.status, [('Content-Type', 'text/plain')])
        dynboot.logger.error(e)
        yield str(e).encode("utf-8")
    except Exception as e:
        start_fn("500 Internal Server Error", [('Content-Type', 'text/plain')])
        dynboot.logger.error(e)
        return [b"Unexpected error.\n"]


def _address(environ):
    try:
        address = http.cookies.SimpleCookie(environ['HTTP_COOKIE'])['_Syslinux_ip'].value.split(':')[0]
        dynboot.logger.debug("[address] Cookie: %s", address)
        return address
    except Exception as e:
        pass

    try:
        address = urllib.parse.parse_qs(environ['QUERY_STRING'])['address'][0]
        dynboot.logger.debug("[address] Query string: %s", address)
        return address
    except Exception as e:
        pass

    return None


def _resolve(address):
    for resolver in dynboot.resolvers:
        dynboot.logger.debug("Resolving %s using %s", address, resolver.__class__.__name__)
        result = resolver.resolve(address)
        if result:
            dynboot.logger.info("Resolved %s using %s", address, resolver.__class__.__name__)
            return result
    dynboot.logger.warn("Failed to resolve %s", address)
    raise NoSuchAddressError(str(address))


def _locate(menu_identifier):
    for locator in dynboot.locators:
        dynboot.logger.debug("Locating %s using %s", menu_identifier, locator.__class__.__name__)
        result = locator.locate(menu_identifier)
        if result:
            dynboot.logger.info("Located %s using %s", menu_identifier, locator.__class__.__name__)
            return result
    dynboot.logger.warn("Failed to locate %s", menu_identifier)
    raise NoSuchMenuError(menu_identifier)
