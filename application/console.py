#!/usr/bin/env python
"""
    console.py
    Cli environment
"""

import click
import logging
import sys
from os.path import abspath, dirname


import dynboot
dynboot.initialize("cli")


@click.group()
def cli():
    sys.path.append(abspath(dirname(sys.argv[0])))


@cli.command()
def cache_clear():
    """
    Clears and warms up cache

    If dynboot.resolver.CacheResolver is being used (at least one of initialized resolvers is CacheResolver)
    flushes all cache entries and warms up cache again using configuration.
    """
    for resolver in dynboot.resolvers:
        if hasattr(resolver, 'clear') and callable(resolver.clear):
            dynboot.logger.info("[cache] %s", resolver.__class__.__name__)
            resolver.clear(dynboot.config)

@cli.command()
def validate():
    """
    Validates configuration files.

    Method does nothing. Configuration is parsed and validated by default
    """
    dynboot.logger.info("Configuration is valid")


@cli.command()
@click.argument('address')
def resolve(address):
    """
    Resolves an IP address

    Resolves an IP address to a menu identifier
    """
    for resolver in dynboot.resolvers:
        dynboot.logger.debug("Resolving %s using %s", address, resolver.__class__.__name__)
        result = resolver.resolve(address)
        if result:
            dynboot.logger.info("Resolved %s using %s", address, resolver.__class__.__name__)
            print(result)
            return
    dynboot.logger.warn("Failed to resolve %s", address)


@cli.command()
@click.argument('menu_identifier')
def locate(menu_identifier):
    """
    Localize a menu by its identifier

    Localizes, reads and outputs menu localized by its identifier.
    """
    for locator in dynboot.locators:
        dynboot.logger.debug("Locating %s using %s", menu_identifier, locator.__class__.__name__)
        result = locator.locate(menu_identifier)
        if result:
            dynboot.logger.info("Located %s using %s", menu_identifier, locator.__class__.__name__)
            print(result)
            return
    dynboot.logger.warn("Failed to locate %s", menu_identifier)

if __name__ == '__main__':
    cli()
