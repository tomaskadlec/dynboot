#!/bin/bash
# Runs dynboot console. If virtualenv does not exist tries to create it.

SCRIPT_FILE=$(python3 -c "import os; print(os.path.realpath('$0'))")
SCRIPT_DIR=$(dirname "$SCRIPT_FILE")

[ ! -d "$SCRIPT_DIR/.venv" -o ! -f "$SCRIPT_DIR/.venv/bin/activate" ] && {
    python3 -m virtualenv -p "$(which python3)" "$SCRIPT_DIR/.venv" ||
    {
        echo "ERROR: Failed to create virtualenv." 1>&2
        exit 1
    }
}        

. "$SCRIPT_DIR/.venv/bin/activate" || {
    echo "ERROR: Failed to activate virtualenv." 1>&2
    exit 2
}

pip -q install -r "$SCRIPT_DIR/requirements.txt" || {
    echo "ERROR: Failed to install requirements." 1>&2
    exit 3

}

exec "$SCRIPT_DIR/console.py" "$@"
