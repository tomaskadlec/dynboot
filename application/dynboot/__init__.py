from dynboot.configuration import Factory
from dynboot.model import Application
from dynboot.exceptions import ApplicationInitError, ConfigurationError, EnvironmentInitError
import logging.config
import pydoc

__author__ = 'tomas@tomaskadlec.net'
__all__ = []

config = Factory().get()
logging.config.dictConfig(config['logging'])
logger = logging.getLogger('dynboot-logger')
resolvers = list()
locators = list()


def initialize(environment):
    logger.debug("Initializing application, environment: {}".format(environment))
    try:
        environment_definition = config['application']['environments'][environment]
        _initialize(environment, environment_definition['resolvers'],
                    config['application']['resolvers'], resolvers, "resolver")
        _initialize(environment, environment_definition['locators'],
                    config['application']['locators'], locators, "locator")
    except ApplicationInitError as e:
        logger.error(e)
        exit(1)
    except Exception as e:
        logger.error("Unexpected exception. " + e)
        exit(2)
    logger.debug("Application initialized, environment: {}".format(environment))


def _initialize(environment, identifiers, definitions, container, type="object"):
    for identifier in identifiers:
        try:
            try:
                params = definitions[identifier]['params']
            except KeyError:
                params = {}
            cls = definitions[identifier]['class']
            logger.debug("Adding %s: %s class: %s", type, identifier, cls)
            cls = pydoc.locate(cls)
            container.append(cls(logger, **params))
        except KeyError as e:
            logger.warning("%s %s is not defined in configuration.", type.capitalize(), identifier)
    if not container:
        logger.error("One %s must exist at least.", type)
        raise EnvironmentInitError(environment)
