"""
    dynboot/exceptions.py
    A collection of exceptions used in dynboot
"""


class ApplicationInitError(Exception):
    def __init__(self, message, *args, **kwargs):
        super().__init__(message.format(*args, **kwargs))


class ConfigurationError(ApplicationInitError):
    pass


class EnvironmentInitError(ApplicationInitError):
    message = "Application failed to initialize, environment: {}."

    def __init__(self, environment):
        super().__init__(self.message.format(environment))
