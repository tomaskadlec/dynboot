"""
    dynboot/locator.py
    Objects used to locate menu file
"""
import abc
from dynboot.configuration import Factory as ConfigFactory
import os


class Locator(object, metaclass=abc.ABCMeta):
    """
    A generic locator class
    """

    def __init__(self, logger):
        """
        @:type logger
        :param logger:
        """
        super().__init__()
        self.logger = logger

    @abc.abstractmethod
    def locate(self, menu_identifier):
        """
        Return menu (string) if found. Otherwise return None
        :param menu_identifier:
        :return: str | None
        """
        pass


class FileLocator(Locator):

    def __init__(self, logger, path="menu"):
        super().__init__(logger)
        if not os.path.isabs(path):
            path = ConfigFactory.dirname() + os.path.sep + path
        self.path = path

    def locate(self, menu_identifier):
        path = self.path + os.path.sep + menu_identifier
        if os.path.isfile(path):
            file = open(path, "r")
            return file.read()
        return None
