"""
    dynboot/resolver.py
    A collection of classes that resolves an IP address to a boot menu
"""

import abc
from dynboot.model import Application
from dynboot import config
import ipaddress
from pymemcache.client.base import Client


class Resolver(object, metaclass=abc.ABCMeta):
    """
    A generic resolver class
    """

    def __init__(self, logger):
        """
        @:type logger
        :param logger:
        """
        super().__init__()
        self.logger = logger

    @abc.abstractmethod
    def resolve(self, address):
        """
        Resolves an IP address to a menu identifier. It resolver fails it returns None
        :param address:
        :return: str | None
        """
        pass


class CacheResolver(Resolver):
    """
    Memcache based resolver
    """

    def __init__(self, logger, server="127.0.0.1", port=11211, **kwargs):
        super().__init__(logger)
        self.client = Client((server, port))

    def resolve(self, address):
        result = self.client.get(address)
        if result:
            result = result.decode('utf-8')
        return result

    def clear(self, config):
        self.logger.info("[cache] \tclear")
        self.client.flush_all()
        self.logger.info("[cache] \twarm up")
        application = Application.get(config)
        for identifier, configuration in application.configurations.items():
            self.logger.info("[cache] \t\t- %s", identifier)
            for address in configuration.group.get_addresses():
                reset = " "
                if self.client.get(str(address)):
                    reset = "!"
                self.logger.debug("Cache entry: %s\t-> %s\t%s\t%s(%d)",
                                  str(address), configuration.menu, reset, identifier, configuration.priority)
                self.client.set(str(address), configuration.menu)
        self.logger.info("[cache] \tdone")


class ConfigurationResolver(Resolver):
    """
    Configuration based resolver
    """

    def __init__(self, logger, **kwargs):
        ":type: dynboot.model.Application"
        super().__init__(logger)
        self.application = Application.get(config)

    def resolve(self, address):
        for configuration in reversed(self.application.configurations.values()):
            if configuration.group.contains(ipaddress.ip_address(address)):
                return configuration.menu
        return None
