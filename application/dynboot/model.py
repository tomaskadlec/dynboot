"""
    dynboot/model.py
    Data model
"""

from abc import ABCMeta, abstractmethod
import collections
import ipaddress
import itertools
import re
from dynboot.exceptions import ConfigurationError

class Application(object):
    """
    Holds application data
    """

    application = None

    @classmethod
    def get(cls, config):
        if not cls.application:
            cls.application = cls(config)
        return cls.application

    def __init__(self, config):
        self.groups = dict()
        self._init_groups(config)
        self.configurations = collections.OrderedDict()
        self._init_configurations(config)

    def _init_groups(self, config):
        # configuration
        for identifier, group_definition in config['groups'].items():
            group = self._init_group(identifier, group_definition)
            self.groups[group.identifier] = group
        # resolve nested groups identifiers to NestedGroup instances
        for identifier, group in self.groups.items():
            if isinstance(group, NestedGroup):
                nested_groups = list()
                for nested_group in group.nested_groups:
                    try:
                        nested_groups.append(self.groups[nested_group])
                    except KeyError:
                        raise ConfigurationError(
                            "Nested group error. {} requires {} which does not exist."
                                .format(identifier, nested_group))
                group.nested_groups = nested_groups

    @staticmethod
    def _init_group(identifier, group_definition):
        group_classes = Group.__subclasses__()
        for group_class in group_classes:
            if group_class.supports(group_definition['type']):
                return group_class(identifier, **group_definition['params'])

    def _init_configurations(self, config):
        # configuration
        configurations = list()
        for identifier, configuration_definition in config['configurations'].items():
            configuration = Configuration(identifier, **configuration_definition)
            try:
                configuration.group = self.groups[configuration_definition['group']]
            except KeyError:
                raise ConfigurationError("Configuration {} defined for a nonexistent group {}."
                                         .format(identifier, configuration_definition['group']))
            configurations.append(configuration)
        # sort configurations by priority (ascending)
        for configuration in sorted(configurations, key=lambda c: c.priority):
            self.configurations[configuration.identifier] = configuration


class Configuration(object):
    """
    Configuration object
    """

    def __init__(self, identifier, menu, group, priority=0):
        """
        :param identifier:
         :type identifier: str
        :param menu:
         :type menu: str
        :param group:
         :type group: dynboot.model.Group
        :param priority:
         :type priority: int
        """
        self.identifier = identifier
        self.menu = menu
        self.group = group
        self.priority = priority


class Group(object, metaclass=ABCMeta):
    """
    Generic group
    """

    def __init__(self, identifier):
        self.identifier = identifier

    @abstractmethod
    def contains(self, address):
        """
        Check if the group contains an IP address
        :param address: An IP address
        :type address: ipaddress.IPv4Address
        :return: true if group contains an IP address false otherwise
        :rtype: bool
        """
        pass

    @abstractmethod
    def get_addresses(self):
        """
        Returns a list of IP addresses the group contains
        :return: A list of IP addresses that the group contains
        """
        pass

    @classmethod
    def supports(cls, type):
        """
        Checks if class supports the type
        :param type: type of a service (e.g. cache, ldap)
        :return: True or False
        """
        if re.search('^{}_group$'.format(type), cls.camel_to_snake(cls.__name__), re.IGNORECASE):
            return True
        else:
            return False

    @staticmethod
    def camel_to_snake(string):
        """
        Converts camel case to snake case
        :param string:
        :return:
        """
        string = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', string)
        string = re.sub('(.)([0-9]+)', r'\1_\2', string)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', string).lower()


class IpNetworkGroup(Group):
    """
    Group of IP addresses defined by network address
    """

    def __init__(self, identifier, network):
        super().__init__(identifier)
        self.network_address = ipaddress.ip_network(network)

    def contains(self, address):
        return address in self.network_address

    def get_addresses(self):
        return self.network_address.hosts()


class IpListGroup(Group):
    """
    Group of IP addresses that are explicitly listed
    """

    def __init__(self, identifier, addresses):
        """
        :param identifier:
        :param addresses: A list of IPv4/IPv6 addresses
        :type addresses: list[str]
        """
        super().__init__(identifier)
        self.addresses = list()
        for address in addresses:
            self.addresses.append(ipaddress.ip_address(address))

    def contains(self, address):
        return address in self.addresses

    def get_addresses(self):
        return iter(self.addresses)


class IpRangeGroup(Group):
    """
    Group of IP addresses that are defined by first and last address
    """

    def __init__(self, identifier, start, end):
        super().__init__(identifier)
        self.start = ipaddress.ip_address(start)
        self.end = ipaddress.ip_address(end)

    def contains(self, address):
        return self.start <= address <= self.end

    def get_addresses(self):
        address = self.start
        while address <= self.end:
            yield address
            address += 1


class NestedGroup(Group):
    """
    Group of groups. It allows indefinite level of nesting in theory (limited by memory).
    """

    def __init__(self, identifier, groups):
        super().__init__(identifier)
        self.nested_groups = groups

    def contains(self, address):
        for nested_group in self.nested_groups:
            if nested_group.contains(address):
                return True
        return False

    def get_addresses(self):
        iterators = []
        for nested_group in self.nested_groups:
            iterators.append(nested_group.get_addresses())
        return itertools.chain(*iterators)
