"""
    dynboot/config/factory.py
    Configuration factory
"""

import json
from jsonschema import validate, Draft4Validator
from jsonschema.exceptions import SchemaError, ValidationError
import os
import sys
import ruamel.yaml


def error_print(*args, **kwargs):
    """
    Prints errors to sdterr
    :param args:
    :param kwargs:
    :return:
    """
    print("[dynboot/config]", *args, file=sys.stderr, **kwargs)


class Factory(object):
    """
    Parses configuration files
    """

    def __init__(self, logger=None):
        """
        Parses and processes configuration objects
        :param logger:
        """
        self.logger = logger
        self.configuration = None

    def get(self, refresh=False):
        """
        Creates or refreshes configuration and returns it
        :return:
        """
        if self.configuration is None or refresh:
            self.configuration = self.load()
            self.validate(self.configuration)
        return self.configuration

    @staticmethod
    def load():
        """
        Loads configuration files
        :return:
        """
        result = dict()
        dirname = Factory.dirname()
        files = [f for f in os.listdir(dirname) if os.path.isfile(os.path.join(dirname, f)) and f.endswith('.yml')]
        error = False
        error_print("[info]", "Loading configuration files: " + str(files))
        for file in files:
            try:
                data = ruamel.yaml.load(open(dirname + os.path.sep + file), Loader=ruamel.yaml.Loader)
                if data and isinstance(data, dict):
                    result = Factory.merge(result, data)
            except Exception as ex:
                error = True
                error_print("[error]", "{}".format(ex))

        if error:
            error_print("[error]", "Failed to load one or more configuration files.")
            sys.exit(1)
        return result

    @staticmethod
    def validate(configuration):
        path = Factory.dirname() + os.path.sep + "schema.json"
        try:
            schema = json.load(open(path, "r"))
            Draft4Validator.check_schema(schema)
            validator = Draft4Validator(schema)
            if not Factory._print_errors(validator.iter_errors(configuration)):
                error_print("[error]", "Configuration is not valid.")
                sys.exit(1)
        except SchemaError as error:
            Factory._print_schema_errors(error)
            sys.exit(1)
        except Exception as error:
            error_print("[error]", "Unexpected error.", error)
            sys.exit(2)

    @staticmethod
    def _print_errors(errors, message="Validation error"):
        valid = True
        for error in errors:
            valid = False
            error_print("[warn]", "-- {} --".format(message))
            error_print("[warn]", "path:", " -> ".join(error.path))
            error_print("[warn]", "message:", error.message)
            error_print("[warn]", "schema:")
            error_print(json.dumps(error.schema, indent=4))
            error_print("[warn]", "----------------------")
        return valid

    @staticmethod
    def _print_schema_errors(error):
        Factory._print_errors([error], "Schema error")

    @staticmethod
    def merge(op1, op2, path=None):
        """
        Merge op1 with op2 and return the result
        :param op1:
        :param op2:
        :param path:
        :return:
        """
        if path is None:
            path = []
        for key in op2:
            if key in op1:
                if isinstance(op1[key], dict) and isinstance(op2[key], dict):
                    Factory.merge(op1[key], op2[key], path + [str(key)])
                    continue
                else:
                    op1[key] = op2[key]
            else:
                op1[key] = op2[key]
        return op1

    @staticmethod
    def dirname():
        return os.path.realpath(os.path.dirname(os.path.realpath(__file__)) +
               os.path.sep + ".." + os.path.sep + os.path.sep + 'config')
