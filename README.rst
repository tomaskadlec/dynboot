dynboot
=======

.. _`Ansible`:
.. _`Docker Engine`:
.. _`Docker Swarm`:

*dynboot* is a simple application written in Python_ that integrates with
pxelinux_ and provides boot menus over HTTP. The process of choosing right menu
for each host is build on top of following notions.


*dynboot* is built on with *groups*, *menus* and *configurations*.

group
    A group of hosts. Membership is based on an IP address of each host. Groups
    may be nested.

menu
    It is a *menu identifier* only. *dynboot* does not manage menus itself it
    only serves them. Typical use case is to store menu in a file named by menu
    identifier.

configuration
    A configuration ties together a *group* and a *menu*. Optionally, a
    *priority* may be provided to prefer one menu over another.

*dynboot* operation consists of two steps. The first one is to map IP address to
*menu identifier*. It walks all *configurations* (sorted by descending priority)
and check, if the IP address is member of related group or not. If it is the
*configuration* and thus *menu* (*menu_identifier* to be precise) is selected.
Otherwise, no *menu* is found and HTTP 404 is returned.

The second step is to find a *menu* based on its *menu identifier*. Typical
scenario is to load and serve a file named same as *menu identifier* from the
server. If this step fails HTTP 500 is returned.

The whole process is shown in the figure that follows:

.. figure:: docs/sequence.png
   :alt: *dynboot operation*

   *dynboot operation*

From the technical perspective, it utilizes uWSGI_ to run the application and
deliver content. 

Supported distributions
-----------------------

Requirements
------------

Installation
------------

Configuration
-------------

Usage
-----


.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
